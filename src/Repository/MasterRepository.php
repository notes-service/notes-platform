<?php

namespace App\Repository;

use App\Entity\Master;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Master>
 *
 * @method Master|null find($id, $lockMode = null, $lockVersion = null)
 * @method Master|null findOneBy(array $criteria, array $orderBy = null)
 * @method Master[]    findAll()
 * @method Master[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MasterRepository extends ServiceEntityRepository
{
    public function __construct(
        public ServiceRepository $serviceRepository,
        public ManagerRegistry   $registry
    ) {
        parent::__construct($registry, Master::class);
    }

    public function save(Master $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Master $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function createMasters($master)
    {
        $services = $this->serviceRepository->findById(preg_replace("/[^0-9]/", '', $master['service']));

        $saveMaster = new Master();
        $saveMaster
            ->addServices($services)
            ->setName($master['name']);

        $this->getEntityManager()->persist($saveMaster);
        $this->getEntityManager()->flush();

        return $saveMaster;
    }
}
