<?php

namespace App\Repository;

use App\Entity\Note;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @extends ServiceEntityRepository<Note>
 *
 * @method Note|null find($id, $lockMode = null, $lockVersion = null)
 * @method Note|null findOneBy(array $criteria, array $orderBy = null)
 * @method Note[]    findAll()
 * @method Note[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NoteRepository extends ServiceEntityRepository
{
    public function __construct(
        public ServiceRepository  $serviceRepository,
        public ManagerRegistry    $registry,
        public PaginatorInterface $paginator
    ) {
        parent::__construct($registry, Note::class);
    }

    public function save(Note $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Note $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getNotes(
        $page,
        $size,
    ): PaginationInterface {
        $notes = $this->createQueryBuilder('n')
            ->select()
            ->orderBy('n.id', 'DESC')
            ->distinct();

        return $this->paginator->paginate($notes->getQuery()->getResult(), $page, $size);
    }

    public function getNotesSinceDate($timeStart, $timeEnd)
    {
        $notes = $this->createQueryBuilder('n')
            ->select('n.datetime,
                            services.total_time
            ')
            ->leftJoin('n.service', 'services')
            ->where('n.datetime >= :start')
            ->andWhere('n.datetime <= :finish')
            ->setParameter('start', $timeStart->format('Y-m-d'))
            ->setParameter('finish', $timeEnd->format('Y-m-d'));

        return $notes->getQuery()->getArrayResult();
    }

    public function createMultipleNotes($note)
    {
        $service = $this->serviceRepository->findOneById(preg_replace("/[^0-9]/", '', $note['service']));

        $saveNote = new Note();
        $saveNote
            ->setService($service)
            ->setBot($note['bot'])
            ->setActive($note['active'])
            ->setEmail($note['email'])
            ->setDescription($note['description'])
            ->setPhone($note['phone'])
            ->setDatetime(new \DateTime($note['datetime']));

        $this->getEntityManager()->persist($saveNote);
        $this->getEntityManager()->flush();

        return $saveNote;
    }
}