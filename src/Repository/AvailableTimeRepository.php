<?php

namespace App\Repository;

use App\Entity\AvailableTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use DateTime;

/**
 * @extends ServiceEntityRepository<AvailableTime>
 *
 * @method AvailableTime|null find($id, $lockMode = null, $lockVersion = null)
 * @method AvailableTime|null findOneBy(array $criteria, array $orderBy = null)
 * @method AvailableTime[]    findAll()
 * @method AvailableTime[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AvailableTimeRepository extends ServiceEntityRepository
{
    public function __construct(
        public ManagerRegistry            $registry,
        public WorkTimeTemplateRepository $workTimeTemplateRepository,
        public NoteRepository             $noteRepository,
        public WeekendRepository          $weekendRepository,
    ) {
        parent::__construct($registry, AvailableTime::class);
    }

    public function save(AvailableTime $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AvailableTime $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getAvailableTime()
    {
        $settingTimes = $this->findAll();
        foreach ($settingTimes as $settingTime) {
            $maxDaysMakeNote          = $settingTime->getMaxDaysMakeNote();
            $timeIntervalBetweenNotes = $settingTime->getTimeIntervalBetweenNotes();
        }
        $notesInDay    = 1440 / $timeIntervalBetweenNotes;
        $format        = 'Y-m-d H:i:s';
        $now           = new DateTime('now');
        $timeStart     = new DateTime('now');
        $timeEnd       = new \DateTimeImmutable('now');
        $iterableTimes = [];
        $response      = [];

        for ($i = 0; $i <= $notesInDay * $maxDaysMakeNote; $i++) {
            $roundTime = $this->roundToMin($now->modify("+$timeIntervalBetweenNotes minutes"), $timeIntervalBetweenNotes);
            $iterableTimes[] = $roundTime->format($format);
        }
        var_dump(count($iterableTimes));
        $availableTimesWithWeekends = $this->getAvailableTimesWithWeekends($iterableTimes, $timeStart);
        $availableTimes = $this->getAvailableTimeWithWorkTimeTemplate($availableTimesWithWeekends);
        $noteObjects = $this->noteRepository->getNotesSinceDate($timeStart, $timeEnd->modify("+$maxDaysMakeNote days"));

        var_dump(count($availableTimes));
        if (isset($noteObjects)) {
            foreach ($noteObjects as $note) {
                $datetime = $note['datetime']->getTimestamp();
                foreach ($this->gen_availableTimes($availableTimes) as $key => $time) {
                    if (isset($availableTimes[$key])) {
                        $availableTime = new \DateTimeImmutable(date($format, $availableTimes[$key]));
                        $serviceTime   = $note['total_time'];
                        $endNote       = $availableTime->modify("+$serviceTime minutes")->getTimestamp();
                        if ($datetime >= $availableTimes[$key] &&
                            $datetime <= $endNote) {
                            unset($availableTimes[$key]);
                        }
                    }
                }
            }
        }

        array_walk_recursive($availableTimes, function ($availableTimes) use (&$response) {
            $response[] = $availableTimes;
        });

        var_dump(count($availableTimes));
        return $response;
    }

    public function roundToMin(\DateTime $dt, $precision = 10) {
        $s = $precision * 60;
        $dt->setTimestamp($s * ceil($dt->getTimestamp() / $s));

        return $dt;
    }

    public function getAvailableTimesWithWeekends($iterableTimes, $timeStart)
    {
        $availableTimeWithWeekends = [];
        $weekends                  = $this->weekendRepository->getWeekendSinceDate($timeStart);
        foreach ($weekends as $weekend) {
            $dateStart = $weekend['date_start']->getTimestamp();
            $dateEnd   = $weekend['date_end']->modify("+1 day")->getTimestamp();
            foreach ($iterableTimes as $iterableTime) {
                if ($dateStart >= strtotime($iterableTime) ||
                    $dateEnd <= strtotime($iterableTime)) {
                    $availableTimeWithWeekends[] = $iterableTime;
                }
            }
        }

        return $availableTimeWithWeekends;
    }

    public function getAvailableTimeWithWorkTimeTemplate($iterableTimes)
    {
        $workTimeTemplates = $this->workTimeTemplateRepository->findAll();
        foreach ($workTimeTemplates as $workTimeTemplate) {
            switch ($workTimeTemplate->getName()) {
                case 'Monday':
                    $monday[] = $workTimeTemplate;
                    break;
                case 'Tuesday':
                    $tuesday[] = $workTimeTemplate;
                    break;
                case 'Wednesday':
                    $wednesday[] = $workTimeTemplate;
                    break;
                case 'Thursday':
                    $thursday[] = $workTimeTemplate;
                    break;
                case 'Friday':
                    $friday[] = $workTimeTemplate;
                    break;
                case 'Saturday':
                    $saturday[] = $workTimeTemplate;
                    break;
                case 'Sunday':
                    $sunday[] = $workTimeTemplate;
                    break;
            }
        }

        $availableTimeWithWorkTimeTemplate = [];
        foreach ($iterableTimes as $iterableTime) {
            $iterableTimeDateTime  = DateTime::createFromFormat('Y-m-d H:i:s', $iterableTime)->format('H:i:s');
            $iterableTimeTimestamp = strtotime('1970-01-01 ' . $iterableTimeDateTime);

            switch (date("l", strtotime($iterableTime))) {
                case 'Monday':
                    foreach ($monday as $time) {
                        if ($time->getWorkTimeStart() !== null && $time->getWorkTimeEnd() !== null) {
                            if ($time->getWorkTimeStart()->getTimestamp() <= $iterableTimeTimestamp &&
                                $time->getWorkTimeEnd()->getTimestamp() >= $iterableTimeTimestamp) {
                                $availableTimeWithWorkTimeTemplate[] = strtotime($iterableTime);
                            }
                        }
                    }
                    break;
                case 'Tuesday':
                    foreach ($tuesday as $time) {
                        if ($time->getWorkTimeStart() !== null && $time->getWorkTimeEnd() !== null) {
                            if ($time->getWorkTimeStart()->getTimestamp() <= $iterableTimeTimestamp &&
                                $time->getWorkTimeEnd()->getTimestamp() >= $iterableTimeTimestamp) {
                                $availableTimeWithWorkTimeTemplate[] = strtotime($iterableTime);
                            }
                        }
                    }
                    break;
                case 'Wednesday':
                    foreach ($wednesday as $time) {
                        if ($time->getWorkTimeStart() !== null && $time->getWorkTimeEnd() !== null) {
                            if ($time->getWorkTimeStart()->getTimestamp() <= $iterableTimeTimestamp &&
                                $time->getWorkTimeEnd()->getTimestamp() >= $iterableTimeTimestamp) {
                                $availableTimeWithWorkTimeTemplate[] = strtotime($iterableTime);
                            }
                        }
                    }
                    break;
                case 'Thursday':
                    foreach ($thursday as $time) {
                        if ($time->getWorkTimeStart() !== null && $time->getWorkTimeEnd() !== null) {
                            if ($time->getWorkTimeStart()->getTimestamp() <= $iterableTimeTimestamp &&
                                $time->getWorkTimeEnd()->getTimestamp() >= $iterableTimeTimestamp) {
                                $availableTimeWithWorkTimeTemplate[] = strtotime($iterableTime);
                            }
                        }
                    }
                    break;
                case 'Friday':
                    foreach ($friday as $time) {
                        if ($time->getWorkTimeStart() !== null && $time->getWorkTimeEnd() !== null) {
                            if ($time->getWorkTimeStart()->getTimestamp() <= $iterableTimeTimestamp &&
                                $time->getWorkTimeEnd()->getTimestamp() >= $iterableTimeTimestamp) {
                                $availableTimeWithWorkTimeTemplate[] = strtotime($iterableTime);
                            }
                        }
                    }
                    break;
                case 'Saturday':
                    foreach ($saturday as $time) {
                        if ($time->getWorkTimeStart() !== null && $time->getWorkTimeEnd() !== null) {
                            if ($time->getWorkTimeStart()->getTimestamp() <= $iterableTimeTimestamp &&
                                $time->getWorkTimeEnd()->getTimestamp() >= $iterableTimeTimestamp) {
                                $availableTimeWithWorkTimeTemplate[] = strtotime($iterableTime);
                            }
                        }
                    }
                    break;
                case 'Sunday':
                    foreach ($sunday as $time) {
                        if ($time->getWorkTimeStart() !== null && $time->getWorkTimeEnd() !== null) {
                            if ($time->getWorkTimeStart()->getTimestamp() <= $iterableTimeTimestamp &&
                                $time->getWorkTimeEnd()->getTimestamp() >= $iterableTimeTimestamp) {
                                $availableTimeWithWorkTimeTemplate[] = strtotime($iterableTime);
                            }
                        }
                    }
                    break;
            }
        }

        return $availableTimeWithWorkTimeTemplate;
    }

    public function getAvailableTimeWeekDays($weekDay, $iterableTimeTimestamp, $iterableTime)
    {
        foreach ($weekDay as $time) {
            if ($time->getWorkTimeStart() !== null && $time->getWorkTimeEnd() !== null) {
                if ($time->getWorkTimeStart()->getTimestamp() <= $iterableTimeTimestamp &&
                    $time->getWorkTimeEnd()->getTimestamp() >= $iterableTimeTimestamp) {
                    $availableTimeWithWorkTimeTemplate[] = strtotime($iterableTime);
                }
            }
        }
        if (isset($availableTimeWithWorkTimeTemplate)) {
            return $availableTimeWithWorkTimeTemplate;
        }

        return null;
    }

    public function gen_availableTimes($availableTimes)
    {
        foreach ($availableTimes as $availableTime) {
            yield $availableTime;
        }
    }

//
}
