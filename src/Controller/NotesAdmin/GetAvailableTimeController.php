<?php

namespace App\Controller\NotesAdmin;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Controller\NotesAdmin\BaseHttpProvider;

class GetAvailableTimeController extends BaseHttpProvider
{
    #[Route('/get-available-time', name: 'get_available_time')]
    public function getNotes()
    {
        $response = $this->request(
            'GET',
            $this->getParameter('get_notes_platform_api_url') . 'available-time',
        );

        return new Response($response->getContent());
    }
}