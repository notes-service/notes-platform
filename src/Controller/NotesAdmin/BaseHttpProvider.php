<?php

namespace App\Controller\NotesAdmin;

use Psr\Cache\CacheItemPoolInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class BaseHttpProvider extends AbstractController
{
    public function __construct(
        public HttpClientInterface    $client,
        public CacheItemPoolInterface $cache,
    ) {}

    public function request(string $method, string $url, array $data = null, array $query = null)
    {
        $requestData = [
            'query' => $query,
            'json'  => $data,
        ];

        $response = $this->client->request($method, $url, $requestData);

        return $response;
    }
}