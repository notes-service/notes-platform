<?php

namespace App\Controller\NotesAdmin;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Controller\NotesAdmin\BaseHttpProvider;

class GetNotesController extends BaseHttpProvider
{
    #[Route('/get-notes', name: 'get-notes')]
    public function getNotes()
    {
        $response = $this->request(
            'GET',
            $this->getParameter('get_notes_platform_api_url') . 'notes',
        );

        return new Response($response->getContent());
    }
}