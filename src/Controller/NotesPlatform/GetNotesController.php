<?php

namespace App\Controller\NotesPlatform;

use App\Repository\NoteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/GetNotes')]
class GetNotesController extends AbstractController
{
    public function __construct(
        protected NoteRepository $noteRepository,
    ) {}

    public function __invoke(Request $request)
    {
        $data     = $request->query->all();
        $page     = $data['Page'] ?? 1;
        $size     = $data['Size'] ?? 100;
        $response = [];

        $notes = $this->noteRepository->getNotes($page, $size);

        foreach ($notes->getItems() as $note) {
            $response[] = [
                'id'          => $note->getId(),
                'active'      => $note->isActive(),
                'phone'       => $note->getPhone(),
                'email'       => $note->getEmail(),
                'bot'         => $note->getBot(),
                'description' => $note->getDescription(),
                'datetime'    => $note->getDatetime(),
                'service'     => [
                    'id'         => $note->getService()->getId(),
                    'name'       => $note->getService()->getName(),
                    'total_time' => $note->getService()->getTotalTime(),
                    'price'      => $note->getService()->getPrice(),
                ]
            ];
        }

        $responseJson = [
            'PageNumber' => $notes->getCurrentPageNumber(),
            'RowCount'   => $notes->getItemNumberPerPage(),
            'TotalCount' => $notes->getTotalItemCount(),
            'Notes'      => $response
        ];

        return $this->json($responseJson);
    }
}