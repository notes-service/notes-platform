<?php

namespace App\Controller\NotesPlatform;

use App\Repository\AvailableTimeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/GetAvailableTime')]
class GetAvailableTimeController extends AbstractController
{
    public function __construct(
        protected AvailableTimeRepository $availableTimeRepository,
    ) {}

    public function __invoke(Request $request)
    {
        $availableTime = $this->availableTimeRepository->getAvailableTime();

        $responseJson = [
            'AvailableTime' => $availableTime
        ];

        return $this->json($responseJson);
    }
}