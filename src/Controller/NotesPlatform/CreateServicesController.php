<?php

namespace App\Controller\NotesPlatform;

use App\Entity\Service;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateServicesController extends AbstractController
{
    public function __invoke(Request $request)
    {
        dd($request->request->get('name'));
        die();
        $service = new Service();
        $service->setName($request->request->get('name'));
        $service->setImageFile($request->request->get('image'));
        $service->setActive($request->request->get('active'));
        $service->setPrice($request->request->get('price'));
        $service->setTotalTime($request->request->get('total_time'));

        return $service;
    }
}