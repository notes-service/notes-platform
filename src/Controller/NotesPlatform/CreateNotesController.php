<?php

namespace App\Controller\NotesPlatform;

use App\Repository\NoteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateNotesController extends AbstractController
{
    public function __construct(
        protected NoteRepository $noteRepository,
    ) {}

    public function __invoke(Request $request)
    {
        $errors = [];
        $responseArray = [];
        $requestBody = $request->getContent(false);
        $requestJson = json_decode($requestBody, true);

        if ($requestBody and json_last_error() !== JSON_ERROR_NONE) { $errors[] = "incorrect json";}

        if (!count($errors)) {
            foreach ($requestJson as $note){
                $this->noteRepository->createMultipleNotes($note);
            }
        }

        $response = new Response();
        $response->setContent(json_encode(array_merge($responseArray, ['errors' => $errors])));
        $response->headers->set('Content-Type', 'application/json');

        return new Response();
    }
}