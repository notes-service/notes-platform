<?php

namespace App\Controller\NotesPlatform;

use App\Repository\ServiceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/GetServices')]
class GetServicesController extends AbstractController
{
    public function __construct(
        protected ServiceRepository $serviceRepository,
    ) {}

    public function __invoke(Request $request)
    {
        $data = $request->query->all();
        $page = $data['Page'] ?? 1;
        $size = $data['Size'] ?? 100;

        $services = $this->serviceRepository->getServices($page, $size);

        $responseJson = [
            'PageNamber' => $services->getCurrentPageNumber(),
            'RowCount'   => $services->getItemNumberPerPage(),
            'TotalCount' => $services->getTotalItemCount(),
            'Services'   => $services->getItems()
        ];

        return $this->json($responseJson);
    }
}