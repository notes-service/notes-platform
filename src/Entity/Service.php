<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Controller\NotesPlatform\CreateServicesController;
use App\Controller\NotesPlatform\GetServicesController;
use App\Repository\ServiceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Array_;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;
use ApiPlatform\Metadata\ApiProperty;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[ORM\Entity(repositoryClass: ServiceRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            name: 'services_get',
            uriTemplate: '/services',
            controller: GetServicesController::class,
        ),
        new Post(
            name: 'services_create',
            uriTemplate: '/services',
            controller: CreateServicesController::class,
            deserialize: false
        ),
        new Put(),
        new Delete()
    ],
)]
#[Vich\Uploadable]
class Service
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column]
    private ?int $total_time = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 19, scale: 2)]
    private ?string $price = null;

    #[ORM\OneToMany(mappedBy: 'service', targetEntity: Note::class)]
    #[Ignore]
    private Collection $note;

    #[ORM\Column]
    private ?bool $active = null;

    #[Vich\UploadableField(mapping: 'media_object', fileNameProperty: 'name')]
    private ?File $imageFile = null;

    #[ORM\ManyToMany(targetEntity: Master::class, mappedBy: 'service')]
    private Collection $masters;


    public function __construct()
    {
        $this->note = new ArrayCollection();
        $this->masters = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTotalTime(): ?int
    {
        return $this->total_time;
    }

    public function setTotalTime(int $total_time): self
    {
        $this->total_time = $total_time;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return Collection<int, Note>
     */
    public function getNote(): Collection
    {
        return $this->note;
    }

    public function addNote(Note $note): self
    {
        if (!$this->note->contains($note)) {
            $this->note->add($note);
            $note->setService($this);
        }

        return $this;
    }

    public function removeNote(Note $note): self
    {
        if ($this->note->removeElement($note)) {
            // set the owning side to null (unless already changed)
            if ($note->getService() === $this) {
                $note->setService(null);
            }
        }

        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @param File|null $imageFile
     */
    public function setImageFile(?File $imageFile): void
    {
        $this->imageFile = $imageFile;
    }

    /**
     * @return Collection<int, Master>
     */
    public function getMasters(): Collection
    {
        return $this->masters;
    }

    public function addMaster(Master $master): self
    {
        if (!$this->masters->contains($master)) {
            $this->masters->add($master);
            $master->addService($this);
        }

        return $this;
    }

    public function removeMaster(Master $master): self
    {
        if ($this->masters->removeElement($master)) {
            $master->removeService($this);
        }

        return $this;
    }
}
