<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Controller\NotesPlatform\GetAvailableTimeController;
use App\Repository\AvailableTimeRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AvailableTimeRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            name: 'available_time',
            uriTemplate: '/available-time',
            controller: GetAvailableTimeController::class,
        ),
        new Post(),
        new Put()
    ],
)]
class AvailableTime
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $max_days_make_note = null;

    #[ORM\Column(nullable: true)]
    private ?int $time_interval_between_notes = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMaxDaysMakeNote(): ?int
    {
        return $this->max_days_make_note;
    }

    public function setMaxDaysMakeNote(int $max_days_make_note): self
    {
        $this->max_days_make_note = $max_days_make_note;

        return $this;
    }

    public function getTimeIntervalBetweenNotes(): ?int
    {
        return $this->time_interval_between_notes;
    }

    public function setTimeIntervalBetweenNotes(?int $time_interval_between_notes): self
    {
        $this->time_interval_between_notes = $time_interval_between_notes;

        return $this;
    }
}
