#!/usr/bin/env bash

# shellcheck disable=SC2034
self="$0"
project_name="$1"
developer_email="$2"
developer_name="$3"

echo -e "ИМЯ ПРОЕКТА: $project_name"

project_directory="/var/www/$project_name"

git config --global user.email "$developer_email"
git config --global user.name "$developer_name"

## Создание проекта
symfony new $project_directory --webapp

## Удаляем ненужные файлы
rm -f $project_directory/{docker-compose.yml,docker-compose.override.yml}

## Если репозиторий клонировали, удаляем папку .git
rm -rf /var/www/.git

## Перемещаем созданный проект Symfony в корневую папку проекта
shopt -s dotglob
mv -f $project_directory/* /var/www/

## Удаляем временную папку
rmdir $project_directory

## Устанавливаем Doctrine ORM и MakerBundle
composer require symfony/orm-pack
composer require --dev symfony/maker-bundle

## Et voila!
