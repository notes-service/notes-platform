#!/usr/bin/env bash

# shellcheck disable=SC2034
self="$0"
project_name="$1"

echo -e "ИМЯ ПРОЕКТА: $project_name"

project_directory="/var/www/$project_name"

## Создание проекта
composer create-project slim/slim-skeleton:dev-master $project_directory

## Если репозиторий клонировали, удаляем папку .git
rm -rf /var/www/.git

## Перемещаем созданный проект Symfony в корневую папку проекта
shopt -s dotglob
mv -f $project_directory/* /var/www/

## Удаляем временную папку
rmdir $project_directory

## Et voila!
