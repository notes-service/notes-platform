#!/usr/bin/env bash

CYAN='\033[0;36m'
GREEN='\033[1;32m'
RED='\033[1;31m'
WHITE='\033[1;37m'
NC='\033[0m'

# shellcheck disable=SC2034
self="$0"
framework="$1"
project_name="$2"
path="$3"

echo -e "${WHITE}Инициализируем проект:${NC} ${CYAN}\"$project_name\" ${WHITE}на базе фреймворка ${CYAN}$framework"

# Создание файла .env по образцу
source="$path.env.example"
target="$path.env"

if [ ! -f "$target" ]; then
  cp -n "$source" "$target"
fi

if [ ! -f "$target" ]; then
  echo -e "${RED}Неизвестная ошибка, не удалось создать файл .env"
fi

echo -e "${WHITE}Создан файл ${CYAN}.env"

function is_gnu_sed(){
  sed --version >/dev/null 2>&1
}

function sed_i_wrapper(){
  if is_gnu_sed; then
    $(which sed) "$@"
  else
    a=()
    for b in "$@"; do
      [[ $b == '-i' ]] && a=("${a[@]}" "$b" "") || a=("${a[@]}" "$b")
    done
    $(which sed) "${a[@]}"
  fi
}

sed_i_wrapper -i "s/example-project/$project_name/g" $target
sed_i_wrapper -i "s/framework-name/$framework/g" $target

echo -e "${NC}${GREEN}"
cat $target
