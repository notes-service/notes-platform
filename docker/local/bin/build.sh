#!/usr/bin/env bash

# shellcheck disable=SC2046
docker run --rm -it -v $(pwd):$(pwd) -w $(pwd) node:14 sh -c "yarn && yarn build"
