<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230519133408 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE master_service (master_id INT NOT NULL, service_id INT NOT NULL, PRIMARY KEY(master_id, service_id))');
        $this->addSql('CREATE INDEX IDX_3DBA77BA13B3DB11 ON master_service (master_id)');
        $this->addSql('CREATE INDEX IDX_3DBA77BAED5CA9E6 ON master_service (service_id)');
        $this->addSql('ALTER TABLE master_service ADD CONSTRAINT FK_3DBA77BA13B3DB11 FOREIGN KEY (master_id) REFERENCES master (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE master_service ADD CONSTRAINT FK_3DBA77BAED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE service_master DROP CONSTRAINT fk_13cea6e0ed5ca9e6');
        $this->addSql('ALTER TABLE service_master DROP CONSTRAINT fk_13cea6e013b3db11');
        $this->addSql('DROP TABLE service_master');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE TABLE service_master (service_id INT NOT NULL, master_id INT NOT NULL, PRIMARY KEY(service_id, master_id))');
        $this->addSql('CREATE INDEX idx_13cea6e013b3db11 ON service_master (master_id)');
        $this->addSql('CREATE INDEX idx_13cea6e0ed5ca9e6 ON service_master (service_id)');
        $this->addSql('ALTER TABLE service_master ADD CONSTRAINT fk_13cea6e0ed5ca9e6 FOREIGN KEY (service_id) REFERENCES service (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE service_master ADD CONSTRAINT fk_13cea6e013b3db11 FOREIGN KEY (master_id) REFERENCES master (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE master_service DROP CONSTRAINT FK_3DBA77BA13B3DB11');
        $this->addSql('ALTER TABLE master_service DROP CONSTRAINT FK_3DBA77BAED5CA9E6');
        $this->addSql('DROP TABLE master_service');
    }
}
