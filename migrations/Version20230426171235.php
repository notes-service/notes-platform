<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230426171235 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE work_time_template ADD work_time_start TIME(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE work_time_template ADD work_time_end TIME(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE work_time_template DROP time_start');
        $this->addSql('ALTER TABLE work_time_template DROP time_end');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE work_time_template ADD time_start VARCHAR(20) DEFAULT NULL');
        $this->addSql('ALTER TABLE work_time_template ADD time_end VARCHAR(20) DEFAULT NULL');
        $this->addSql('ALTER TABLE work_time_template DROP work_time_start');
        $this->addSql('ALTER TABLE work_time_template DROP work_time_end');
    }
}
