# Makefile

## Подключение основного файла Makefile
-include ./docker/local/Makefile

## Подключение файла Makefile c командами CLI Symfony
-include ./docker/local/SymfonyMakefile

## Подключение файла Makefile c командами CLI Laravel
#-include ./docker/local/LaravelMakefile
