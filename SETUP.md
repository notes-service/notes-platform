# Шаблон Docker-окружения для проектов на базе Symfony6/Laravel9

## Создание нового проекта

### Первоначальная установка

- Скачайте из репозитория архив данного проекта (не клонируйте репозиторий)
- Разархивируйте содержимое в папку проекта
- Запустите команду `make create-config`, которая создаст файл `docker/local/Makefile.config`
- Откройте данный файл на редактирование и измените параметры:
    - `FRAMEWORK` - `symfony` или `laravel`
    - `PROJECT_NAME` - введите имя проекта латинскими буквами в нижнем регистре (например, `my-project`)
    - `DEVELOPER_EMAIL` - введите ваш рабочий адрес почты
    - `DEVELOPER_NAME` - введите ваше имя транслитом
- Запустите команду `make init`, которая создаст файл `docker/local/.env`
- Просмотрите настройки в файле, убедитесь, что параметры применены верно
- Запустите команду `make up`, команда соберет и запустит необходимые контейнеры
- Запустите команду `make framework-install`, команда установит фреймворк, зависимости и развернет чистое приложение
- Команда `make add-host` добавит в ваш файл `/etc/hosts` строку вида `127.0.0.1 my-project.test` (будет использовано указанное вами имя проекта) 

Уже можно открыть в браузере адрес `имя-вашего-проекта.test` и убедиться в работоспособности созданного окружения.

## Настройка Symfony

### PostgreSQL

Установите в следующей строке выбранное вами имя проекта и добавьте в файл `.env`:

```ini
DATABASE_URL="postgresql://имя-вашего-проекта:s3cr3t@database:5432/имя-вашего-проекта?serverVersion=13&charset=utf8"
```

### RabbitMQ

Установите модуль `messenger` при помощи команды:

```shell
composer require symfony/messenger
```

Добавьте в `.env` строку:

```ini
MESSENGER_TRANSPORT_DSN="amqp://rabbitmq:s3cr3t@imdb-dataset.test:5672/%2f/messages"
```

### Mailhog

Установите пакет mailer с помощью команды:

```shell
composer require symfony/mailer
```

Подключите сервис, указав строку DSN в файле `.env`:

```ini
MAILER_DSN=smtp://mailhog:1025
```

## Настройка Laravel

### PostgreSQL

Укажите в файле `.env` следующие параметры:

```ini
DB_CONNECTION=pgsql
DB_HOST=database
DB_PORT=5432
DB_DATABASE="имя-вашего-проекта"
DB_USERNAME="имя-вашего-проекта"
DB_PASSWORD=s3cr3t
```

### RabbitMQ

Laravel из коробки не умеет работать с RabbitMQ, у вас есть возможность использовать следующие пакеты:

- [RabbitMQ Queue driver for Laravel](https://github.com/vyuldashev/laravel-queue-rabbitmq)
- [php-amqplib](https://github.com/php-amqplib/php-amqplib)
- [bschmitt/laravel-amqp](https://github.com/bschmitt/laravel-amqp)

### Mailhog

Для подключения сервиса Mailhog для отладки отправки почты, укажите в файле `.env` следующие параметры:

```ini
MAIL_DRIVER=smtp
MAIL_HOST=mailhog
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=from@example.com
MAIL_FROM_NAME="${APP_NAME}"
```

